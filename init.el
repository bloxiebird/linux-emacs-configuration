﻿;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

;; Packages

;; Use-Package

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Read the Org Configuration
(when (file-readable-p "~/.emacs.d/config.org")
  (org-babel-load-file (expand-file-name "~/.emacs.d/config.org")))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#d2ceda" "#f2241f" "#67b11d" "#b1951d" "#3a81c3" "#a31db1" "#21b8c7" "#655370"])
 '(custom-safe-themes
   '("7546a14373f1f2da6896830e7a73674ef274b3da313f8a2c4a79842e8a93953e" "d6603a129c32b716b3d3541fc0b6bfe83d0e07f1954ee64517aa62c9405a3441" "8f5a7a9a3c510ef9cbb88e600c0b4c53cdcdb502cfe3eb50040b7e13c6f4e78e" "ba72dfc6bb260a9d8609136b9166e04ad0292b9760a3e2431cf0cd0679f83c3a" "e1ecb0536abec692b5a5e845067d75273fe36f24d01210bf0aa5842f2a7e029f" "be9645aaa8c11f76a10bcf36aaf83f54f4587ced1b9b679b55639c87404e2499" "8a379e7ac3a57e64de672dd744d4730b3bdb88ae328e8106f95cd81cbd44e0b6" "41098e2f8fa67dc51bbe89cce4fb7109f53a164e3a92356964c72f76d068587e" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" "2035a16494e06636134de6d572ec47c30e26c3447eafeb6d3a9e8aee73732396" "86704574d397606ee1433af037c46611fb0a2787e8b6fd1d6c96361575be72d2" "a41b81af6336bd822137d4341f7e16495a49b06c180d6a6417bf9fd1001b6d2b" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default))
 '(fci-rule-color "#a28c6f")
 '(hl-todo-keyword-faces
   '(("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#3a81c3")
     ("OKAY" . "#3a81c3")
     ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f")
     ("DONE" . "#42ae2c")
     ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d")
     ("HACK" . "#b1951d")
     ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f")
     ("XXX+" . "#dc752f")
     ("\\?\\?\\?+" . "#dc752f")))
 '(jdee-db-active-breakpoint-face-colors (cons "#030100" "#A76A19"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#030100" "#813c07"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#030100" "#100e0d"))
 '(objed-cursor-color "#874804")
 '(package-selected-packages
   '(ranger omnisharp csharp-mode bug-hunter ewal-doom-themes ewal-evil-cursors ewal-spacemacs-themes ewal org-bullets company flyspell-correct-ivy evil ivy which-key use-package))
 '(pdf-view-midnight-colors '("#655370" . "#fbf8ef"))
 '(rustic-ansi-faces
   ["#040201" "#874804" "#813c07" "#a45101" "#A76A19" "#D76C03" "#F09010" "#e8c99f"])
 '(vc-annotate-background "#040201")
 '(vc-annotate-color-map
   (list
    (cons 20 "#813c07")
    (cons 40 "#8c4305")
    (cons 60 "#984903")
    (cons 80 "#a45101")
    (cons 100 "#9d4f02")
    (cons 120 "#964d03")
    (cons 140 "#8F4C05")
    (cons 160 "#a75604")
    (cons 180 "#bf6103")
    (cons 200 "#D76C03")
    (cons 220 "#bc6003")
    (cons 240 "#a15403")
    (cons 260 "#874804")
    (cons 280 "#693906")
    (cons 300 "#4b2b08")
    (cons 320 "#2d1c0a")
    (cons 340 "#a28c6f")
    (cons 360 "#a28c6f")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

